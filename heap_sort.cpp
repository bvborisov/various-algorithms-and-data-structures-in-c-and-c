#include "stdafx.h"
#include "heap_sort.h"
#define TESTING_HS 0

#if TESTING_HS
int main()
{
  int size;
#if 1
  // mini testing
  std::vector<int> A;


  A.push_back(1);
  A.push_back(5);
  A.push_back(2);
  A.push_back(10);
  A.push_back(4);
  
  std::cout<<"Size of A is :"<<A.end() - A.begin()<<"\n";
  MaxHeap::heap_sort(A.begin(),A.end());
  for(auto x: A) std::cout<< x << " ";
  std::cout<<"\n";
#endif

#if 1
  // big testing
  std::vector<int> B;
  std::vector<int> C;
  
  size = 50;
  for(int i=0; i < size; i++){
    auto rand = random();
    B.push_back(rand % (3*size/2));
    C.push_back(rand % (3*size/2));
  }
  
  MaxHeap::heap_sort(B.begin(),B.end());
  std::sort(C.begin(),C.end());
  std::cout<<"Heap sort works ok? : "<<(B==C)<<"\n";
#endif

#if 1
  // benchmarking
  std::vector<int> D;
  std::vector<int> E;
  
  size = 100000000;
  for(int i=0; i < size; i++){
    auto rand = random();
    //D.push_back(rand % (3*size/2));
    E.push_back(rand % (3*size/2));
  }
  
  //MaxHeap::heap_sort(D.begin(),D.end());
  std::sort(E.begin(),E.end());

#endif

  return 0;
}
#endif

// old code
#if 0
class MaxHeapInd{
public:
  inline int parent_index(int i) { return (i-1)/2; };
  inline int left_index(int i) { return 2*i +1; };
  inline int right_index(int i) { return 2*i + 2; };
  inline void decr_heap() { heap_size--; } ;

  /*
    we pass the array A by reference. However, we need to hold
    the internal, heap array A, as a pointer to the reference,
    since if we had 
    this->A = A  instead of the current this->A = &A
    our internal array A would become a copy of the argument A.
    I suspect this is because of how operator= is defined for
    std::vector which makes sence i.e. copying some object
    should return a new object instead of a reference to the same one. (that would 
    defeat the purpose of copying altogether...).
    When MaxHeap.A is a std::vector<int>* this problem is solve
  */
  void set_array(std::vector<int>& A){ 
    this->A = &A; // 
    this->heap_size = A.size();
  }

  std::vector<int>* get_array() { return A; };

  void heapify(int i)
  {
    int l = left_index(i);
    int r = right_index(i);
    int largest;
    
    if(l < heap_size && A->at(l) > A->at(i)){
      largest = l;
    }
    else largest = i;
    if(r < heap_size && A->at(r) > A->at(largest)){
      largest = r;
    }
    if (largest != i){
      int tmp = A->at(i);
      A->at(i) = A->at(largest);
      A->at(largest) = tmp;
      heapify(largest);
    }
  }
  static void build_heap(std::vector<int>& A)
  {
    MaxHeap heap(A);
    for(int i = A.size()/2-1; i >= 0; i--){
      heap.heapify(i);
    }
  }
  
  static void heap_sort(std::vector<int>& A)
  {
    // build heap
    MaxHeap heap(A);
    for(int i = A.size()/2-1; i >= 0; i--){
      heap.heapify(i);
    }
    for(int i = A.size() - 1; i >= 1; i--){
      int tmp = heap.get_array()->at(0);
      heap.get_array()->at(0) = heap.get_array()->at(i);
      heap.get_array()->at(i) = tmp;
      heap.decr_heap();
      heap.heapify(0);
    }
  }

private:
  MaxHeap(std::vector<int>& A) { set_array(A); };
  void operator=(MaxHeap&);
  MaxHeap(const MaxHeap&);
  ~MaxHeap() {};
  std::vector<int> A;

  int heap_size;
};
#endif
