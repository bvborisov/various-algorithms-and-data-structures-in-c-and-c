#include "stdafx.h"
#ifndef H_SORTER_H
#define H_SORTER_H

template<class T> class Sorter{
protected:
  typedef T value_type ;
  typedef typename std::vector<T>::iterator iterator_type;
  static void sort(iterator_type begin, iterator_type end);
  Sorter(){};
  ~Sorter(){};
  void operator= (Sorter& other){};
  Sorter (const Sorter& other){};
};


#endif
