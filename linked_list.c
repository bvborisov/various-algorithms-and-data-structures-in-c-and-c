#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

//TODO - implement it either with void*p or template it

typedef struct node{
  int el;
  struct node* next;
} node;

node* llfa(int a[], int n)
{
  if (!a)
    return NULL;
  node* l = (node*) malloc(sizeof(node));
  node* r = l;
  for(int i = 0; i < n-1; i++){
    l->el = a[i];
    node* next = (node*) malloc(sizeof(node));
    if(next != NULL){
      l->next = next;
      l = next;
    }
  }
  l->el = a[n-1];
  l->next = NULL;
  return r;
}

/*
  uses array - slow but ok for testing purposes
 */
node* create_random_list (int n, int m)
{
  if (!n)
    return NULL;
  int* a = (int*) malloc (__NUM*sizeof(int));
  if (!a)
    return NULL;
  for (int i = 0; i < n; ++i)
    *(a+i) = random() % m;
  node* l = llfa(a, n);
  free(a);
  return l;
}
/*
  reverses a linked list and returns a pointer to its first element
  Linear time loop implementation
*/
node* reverse(node* l)
{
  node* previous = NULL;
  while(l != NULL){
    node* Next = l->next;
    l->next = previous;
    previous = l;
    l = Next;
  }
  return previous;
}
/*
  reverses a linked with and returns a pointer to its first element
  Recursive implementation, the second parameter passed in is would
  contain the built in array
*/
node* rreverse(node* l, node* ret)
{
  if(l == NULL) return ret;
  else{
    node* next = l->next;
    l->next = ret;
    return rreverse(next,l);
  }
}

void printll(node* l)
{
#ifdef __DEBUG
  while(l != NULL){
    printf("%d ",l->el);
    l = l->next;
  }
  printf("\n");
#endif
}

#ifdef __DEBUG
#define __DEBUG_CODE 1
#else
#define __DEBUG_CODE 0
#endif

#define dprintf(...) do { if (__DEBUG_CODE) printf(__VA_ARGS__);} while (0)


// append to front
// assume data is integer
// assume node has next and data
node* append(node* l, int d){
    node* nn = (node*) malloc(sizeof(node));
    if (!nn) return l;
    if (!l) return l;
    nn->el = d;
    nn->next = l;
    return nn;
}
node* create_list (int el);
node* appendTail(node* l, int d){
  node* nn = create_list (d);
  if (!l)
    return nn;
  while(l->next){
    l = l->next;
  }
  l->next = nn;
  return nn;
}

// cracking the code interview
node* create_list (int el)
{
  node *list = (node*) malloc (sizeof (node));
  if (!list)
    return NULL;
  list->el = el;
  list->next = NULL;
  return list;
}


node* unlink_and_ret (node* n)
{
  if (!n) return NULL;
  node* ret = n->next;
  n->next = NULL;
  free(n);
  return ret;
}


void destroy_list (node* list)
{
  while(list)
    {
      list = unlink_and_ret (list);
    }
}
// 2.1
/*
  destroy node pointed to by p. Returns
  (new) start of list.
 */
node* delete_p (node* list, node* p)
{
  node* list_start = list;
  if (!p || !list) 
    return list;
  if (list == p)
    {
      return unlink_and_ret (list);
    }
  while (list->next != p)
    list = list->next;
  list->next = p->next;
  unlink_and_ret (p);
  return list_start;
}

// 2.1
void delete_el (node* list, int el)
{
  if (list->el == el)
    list = unlink_and_ret (list);
  while (list->next)
    {
      while (list->next && list->next->el != el)
	list = list->next;
      delete_p (list, list->next);
    }
}


// 2.2
int len (node* list)
{
  // base case
  if (!list)
    return 0;
  int len = 1;
  // loop
  while (list->next)
    {
      ++ len;
      list = list->next;
    }
  return len;
}

node* find_nth (node* list, int n)
{
  if (!list)
    return NULL;
  if (!list->next)
    return list;
  // loop
  int c = 0;
  while (list->next && c < n)
    {
      list = list->next;
      ++c;
    }
  if (c != n)
    return NULL;
  return list;
}

node* find_nth_to_last (node* list, int n)
{
  int length = len (list);
  return find_nth (list, length - n - 1);
}

// 2.3 

void delete_value (node* list)
{
  // base
  if (!list)
    return;
  // loop
  node* list_p;
  while (list->next)
    {
      // swap values of node with next node
      int tmp = list->el;
      list->el = list->next->el;
      list->next->el = tmp;
      list_p = list;
      list = list->next;
    }
  list_p->next = NULL;
  free(list);
}

// 2.4

node* sum_lists (node* list1, node* list2)
{
  // base
  if (!list1)
    return list2;
  if (!list2)
    return list1;

  int c = 0;
  int s = 0;
  int el1, el2;
  node* new_list_tail = NULL;
  node* new_list_head = NULL;
  while (list1 || list2 || c)
    {
      if (!list1) 
	el1 = 0;
      else
	{
	  el1 = list1->el;
	  list1 = list1->next;
	}
      if (!list2) 
	el2 = 0;
      else
	{
	  el2 = list2->el;
	  list2 = list2->next;
	}

      s = (c + el1 + el2) % 10;
      c = (c + el1 + el2) / 10;
      new_list_tail = appendTail (new_list_tail, s);
      if (!new_list_head)
	new_list_head = new_list_tail;
    }
  return new_list_head;
}

int main()
{
#ifndef __NUM
#define __NUM 10
#endif
  srandom(time(NULL));
  
  node* l = create_random_list (__NUM, 10);
  l = append(l,8);
  appendTail(l,9);
  appendTail(l,9);

  dprintf("printing linked list...\n");
  printll(l);

  delete_el(l,13);
  dprintf("printing linked list...\n");
  printll(l);
  
  dprintf("Reversing and printing linked list...\n");
  l = reverse(l);

  printll(l);
  l = rreverse(l,NULL);
  dprintf("Reversing and printing again linked list...\n");
  printll(l);
  
  dprintf ("Deleting all elements equal to 2\n");
  delete_el (l, 2);
  dprintf("printing linked list...\n");
  printll(l);
  
  dprintf ("print length.\n");
  dprintf ("list has length %d\n", len (l));

  dprintf ("Find %dth element\n", 4);
  node* l_4th = find_nth (l, 4);
  printll (l_4th);

  dprintf ("Find %dth to last\n", 2);
  node* l_2nd_tolast = find_nth_to_last (l, 2);
  printll (l_2nd_tolast);
  
  dprintf ("Destroy element 1( counting from 0) element only\n");
  delete_value (l->next);
  dprintf("printing linked list...\n");
  printll(l);


  node* l2 = create_random_list (__NUM,10);
  
  node* l3 = NULL;
  dprintf ("printing 2 lists\n");
  printll(l);
  printll(l2);
  l3 = sum_lists (l, l2);
  dprintf ("print summed lists.\n");
  printll(l3);

  dprintf ("Destroy list.\n");
  destroy_list (l);

  dprintf ("Destroy list.\n");
  destroy_list (l2);

  return 0;
}
